
name := "NRTProcessor"

resolvers += Resolver.sonatypeRepo("public")
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.3.2" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.3.2" % "provided",
  "org.apache.spark" %% "spark-streaming" % "2.3.2" % "provided",
  "org.apache.hadoop" % "hadoop-aws" % "2.8.5",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.3.2",
  "org.apache.spark" % "spark-sql_2.11" % "2.3.2",
  "org.apache.kafka" % "kafka-clients" % "2.0.0",
  "org.apache.spark" %% "spark-hive" %   "2.3.2" % "provided",
  "com.memsql" %% "memsql-connector" % "2.0.6",
//  "RedisLabs" % "spark-redis" % "0.3.2" from  "https://dl.bintray.com/spark-packages/maven/RedisLabs/spark-redis/0.3.2/spark-redis-0.3.2.jar",
  "com.redislabs" % "spark-redis" % "2.3.1-M2" from "https://oss.sonatype.org/content/repositories/staging/com/redislabs/spark-redis/2.3.1-M2/spark-redis-2.3.1-M2.jar",
  "redis.clients" % "jedis" % "2.9.0",
  "mysql" % "mysql-connector-java" % "8.0.9-rc",
  "net.liftweb" %% "lift-json" % "3.3.0-RC1"
)

assemblyMergeStrategy in assembly := {
  case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

assemblyJarName in assembly := "nrt.jar"
