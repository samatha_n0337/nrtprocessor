package com.myntra.streaming

import com.myntra.streaming.config.Credential
import com.redislabs.provider.redis.{RedisConfig, RedisEndpoint}

/*
Created By samathashetty on 06/11/18
*/
object RedisProp {

  def getConfig(credential: Credential):RedisConfig={

    val redisServerDnsAddress = credential.url
    val redisPortNumber = credential.port.toInt
    val redisPassword = credential.password//foobared"
    new RedisConfig(new RedisEndpoint(redisServerDnsAddress, redisPortNumber, redisPassword, timeout = 8000))

  }




}
