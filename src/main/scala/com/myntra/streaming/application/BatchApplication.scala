package com.myntra.streaming.application

import java.text.SimpleDateFormat
import java.util.Calendar

import com.myntra.streaming.config.{Config, ConfigFactory, Schema}
import com.myntra.utils.{DBUtils, JsonParser}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{IntegerType, StringType, TimestampType}

/*
Created By samathashetty on 25/11/18
*/
object BatchApplication {

  def calcClickOrImp(eventType: String): String = {
    if (List("streamCardChildClick", "streamCardClick").contains(eventType))
      "clicks"
    else
      "impressions"
  }

  val eventType = udf((eventType: String) => calcClickOrImp(eventType))

  def changeFormat(date: String, currentFormat: String, finalFormat: String): String = {
    val dateCurr =
      if (date != null)
        new SimpleDateFormat(currentFormat).parse(date)
      else {
        Calendar.getInstance().getTime
      }
    new SimpleDateFormat(finalFormat).format(dateCurr)
  }

  val dateChangeFormat = udf((date: String, currentFormat: String, finalFormat: String) => changeFormat(date, currentFormat, finalFormat))


  def main(args: Array[String]): Unit = {

    val sConf = new SparkConf()

    val configPath = args(0)

    val config: Config = JsonParser.parseJson(configPath)

    val credentials = config.credentialMap

    val session = ConfigFactory.getSparkSession
    val eventCountDf = session.read.option("delimiter", ",").schema(Schema.eventsSchema).csv("s3://madlytics/NRT/events_count.csv")
      .select(
        col("refId"),
        eventType(col("event_type")) as "clicksImpressions",
        col("count")
      )
      .groupBy("refId").pivot("clicksImpressions").agg(
      sum("count") as "count"
    )


    val df = session.read.format("parquet").schema(Schema.folBatchSchema).load("s3://madlytics/udpAggregates/sources/revenue_attribution_dim_style/y=2018/m=11/d=23/").select(
      col("ref_widget_whom") as "refId",
      col("item_revenue_inc_Cashback"),
      col("order_created_date")
    ).filter("refId is not null")
      .groupBy("refId")
      .agg(
        sum("item_revenue_inc_Cashback") as "revenue",
        last("order_created_date") as "order_created_date"
      )

    val nrt_batch_out = df.join(eventCountDf, df("refId") === eventCountDf("refId"), "full_outer")
      .select(
        coalesce(df("refId"), eventCountDf("refId")) as "refId",
        lit("Android") as "os",
        lit(1) as "is_logged_in",
        coalesce(col("clicks"), lit(0)) as "clicks",
        coalesce(col("impressions"), lit(0)) as "impressions",
        coalesce(col("revenue"), lit(0.0)) as "revenue",
        dateChangeFormat(col("order_created_date"), lit("yyyyMMdd"), lit("yyyyMMdd")).cast(IntegerType) as "date",
        dateChangeFormat(col("order_created_date"), lit("yyyyMMdd"), lit("YYYY")).cast(IntegerType) as "year",
        dateChangeFormat(col("order_created_date"), lit("yyyyMMdd"), lit("HH")).cast(IntegerType) as "hour",
        dateChangeFormat(col("order_created_date"), lit("yyyyMMdd"), lit("mm")).cast(IntegerType) as "min",
        dateChangeFormat(col("order_created_date"), lit("yyyyMMdd"), lit("MMMM")).cast(StringType) as "month",
        lit(null).cast(TimestampType) as "last_modified_on"
      )


    DBUtils.writeToMemSql(nrt_batch_out, SaveMode.Overwrite, "nrt_admetrics_lifetime")
  }


}
