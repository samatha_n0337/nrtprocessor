package com.myntra.streaming.application

import java.util.concurrent.TimeUnit

import com.myntra.streaming.application.BatchApplication.dateChangeFormat
import com.myntra.streaming.config.{Config, ConfigFactory}
import com.myntra.streaming.processor.{KafkaEventsProcessor, SalesProcessor}
import com.myntra.streaming.writer.DataStreamWriterUtils
import com.myntra.utils.TimeConstants._
import com.myntra.utils.Utils._
import com.myntra.utils.{Constants, JsonParser, Utils}
import net.liftweb.json.parse
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, TimestampType}

/*
Created By samathashetty on 22/10/18
*/
object MainApplication {

  var utils  = Utils.instance()
  
  def main(args: Array[String]): Unit = {

    val configPath = args(0)


    val conf = new Configuration()
    val fileSystem = FileSystem.get(conf)
    implicit val formats = net.liftweb.json.DefaultFormats
    val path = new Path(configPath)
    val json = parse(scala.io.Source.fromInputStream(fileSystem.open(path)).mkString)
    val config = json.extract[Config]



    //    val config: Config = JsonParser.parseJson(configPath)

    val credentials = config.credentialMap
    ConfigFactory.initSparkSession(new SparkConf())
    val session = ConfigFactory.getSparkSession
    session.sparkContext.addJar("hdfs:///user/hadoop/mysql/mysql-connector-java-8.0.12.jar")

    println("config:::" + config.appName)
    credentials.foreach(f => {
      println(f._1 + ":" + f._2.url + ":" + f._2.dbName)
    })

    val rawEvents = KafkaEventsProcessor.init(credentials("madlytics_kafka"))

    val eventsToRedis = rawEvents.filter(s"entity_id is not null and entity_id rlike '${Constants.REF_ID_REGEX}'")
      .select(
        col("session_id") as "key",
        col("entity_id") as "value"
      )

    //Save the sessionId mapped to the refId
    DataStreamWriterUtils.writeToRedis(eventsToRedis, credentials("nrt_redis"),  "sessionRefId", OutputMode.Append(), 5, TimeUnit.MINUTES)


    // Map the refId to all the matched sessions_ids
    val eventsWithRefId = rawEvents
      .withColumn("refId", refIdMapping(col("entity_id"), col("session_id")))
      .withColumn("revenue",  fillRevenue(col("uidx"), col("style_id")).cast(DoubleType))


    val eventsGroupedByDim = eventsWithRefId.groupBy(
//      window(col("timestamp"), "5 minutes"),
      col("refId"), col("is_logged_in"), col("os"), col("bucket")
    ).agg(
      sum("click") as "clicks",
      sum("Impressions") as "impressions",
      sum("revenue") as "revenue"
    )


    //save the clicks and impressions to memsql
    val eventsToMemSql = eventsGroupedByDim.filter("refId is not null")// and (clicks > 0 or impressions > 0 or revenue > 0.0)")
      .select(
        col("refId") as "ref_id",
        col("os"),
        col("is_logged_in"),
        col("bucket"),
        lit(1) as "platform_id", //fix
        col("clicks") as "clicks",
        col("impressions") as "impressions",
        col("revenue") as "revenue",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit(YYYY_MM_DD)).cast(IntegerType) as "date",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit(YYYY)).cast(IntegerType) as "year",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit(MMMM)).cast(StringType) as "month",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit(HOUR_24)).cast(IntegerType) as "hour",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit(MM)).cast(IntegerType) as "min",
        lit(null).cast(TimestampType) as "last_modified_on"
      )//.sort("ref_id", "os", "is_logged_in", "bucket")
    //write to memsql
    DataStreamWriterUtils.writeJdbc(eventsToMemSql, "/tmp/demo-checkpoint-fol/", credentials("nrt_memsql"), "Update", 5, TimeUnit.MINUTES, false)

    val eventsGroupedToRedis = eventsWithRefId
      .select("style_id", "uidx", "refId", "os", "is_logged_in", "bucket")
      .dropDuplicates
      .filter("style_id is not null")
      .select(
        concat(col("uidx"), lit("%"), col("style_id")) as "key",
        concat(col("refId"), lit("%"), col("os"), lit("%"), col("is_logged_in"), lit("%"), col("bucket")) as "value"
      )

    //map the uidx- styleId to the dimensions
    DataStreamWriterUtils.writeToRedis(eventsGroupedToRedis, credentials("nrt_redis"), "eventsEarly", OutputMode.Append(), 5, TimeUnit.MINUTES)

    val folStyleId = SalesProcessor.init

    val folToMemSql = folStyleId.filter("dimensionFields is not null").
      groupBy("dimensionFields")
        .agg(
          first("platform_id") as "platform_id",
          sum("revenue") as "revenue"
        ).
      select(
      indexFromConcatField(col("dimensionFields"), lit("%"), lit(0)) as "ref_id",
      indexFromConcatField(col("dimensionFields"), lit("%"), lit(1)) as "os",
      indexFromConcatField(col("dimensionFields"), lit("%"), lit(2)) as "is_logged_in",
      indexFromConcatField(col("dimensionFields"), lit("%"), lit(3)) as "bucket",
      col("platform_id"),
      lit(0) as "clicks",
      lit(0) as "impressions",
      col("revenue").cast(DoubleType))
      .select(
        col("ref_id"),
        col("os"),
        col("is_logged_in"),
        col("bucket"),
        col("platform_id"),
        col("clicks"),
        col("impressions"),
        col("revenue"),
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit("yyyyMMdd")).cast(IntegerType) as "date",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit("YYYY")).cast(IntegerType) as "year",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit("MMMM")).cast(StringType) as "month",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit("HH")).cast(IntegerType) as "hour",
        dateChangeFormat(col("bucket"), lit("yyyyMMddHHmmss"), lit("mm")).cast(IntegerType) as "min",
        lit(null).cast(TimestampType) as "last_modified_on"
      )//.sort("ref_id", "os", "is_logged_in", "bucket")

    DataStreamWriterUtils.writeJdbc(folToMemSql, "/tmp/fol-checkpoint-fol/", credentials("nrt_memsql"),
      "Update", 15, TimeUnit.MINUTES, true)

    val folRedis = folStyleId.filter("dimensionFields is null")
      .groupBy("uidx", "styleId")
      .agg(
        sum("revenue") as "revenue"
      )
        .select(
          concat(col("uidx"), lit("%"), col("styleId")) as "key",
          coalesce(col("revenue"), lit(0.0)).cast(StringType) as "value"
        )

    DataStreamWriterUtils.writeToRedis(folRedis, credentials("nrt_redis"), "revenueEarly", OutputMode.Complete(),15, TimeUnit.MINUTES)


    session.streams.awaitAnyTermination()


  }

  val fillEvents = udf((uidx: String, style_id: String) => {

    if (utils.eventsMap == null || utils.eventsMap.isEmpty|| !utils.eventsMap.containsKey(uidx + "%" + style_id))
      utils.populateRedisMap()

    utils.eventsMap.getOrDefault(uidx + "%" + style_id, null)

  })

  val fillRevenue = udf((uidx: String, style_id: String) => {

    if (utils.revenueMap == null || utils.revenueMap.isEmpty|| !utils.revenueMap.containsKey(uidx + "%" + style_id))
      utils.populateRedisMap()

    utils.revenueMap.getOrDefault(uidx + "%" + style_id, "0.0")


  })


  val refIdMapping = udf((refId: String, sessionId: String) => getRefId(refId, sessionId))

  def getRefId(refId: String, sessionId: String): String = {
    if (refId != null)
      refId
    else {
      if (utils.refIdMap == null || utils.refIdMap.isEmpty || !utils.refIdMap.containsKey(refId))
        utils.populateRedisMap()

      utils.refIdMap.getOrDefault(sessionId, null)
    }
  }



}
