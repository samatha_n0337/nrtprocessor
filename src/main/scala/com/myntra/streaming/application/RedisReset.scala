package com.myntra.streaming.application

import com.myntra.streaming.RedisProp
import com.myntra.streaming.config.{Config, ConfigFactory, Schema}
import com.myntra.streaming.processor.KafkaEventsProcessor.{style_id, timeToBinTime}
import com.myntra.streaming.reader.{InputReaderFactory, SparkReader}
import com.myntra.utils.JsonParser
import org.apache.spark.sql.functions._
import com.redislabs.provider.redis._
import net.liftweb.json.parse
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.types.{StringType, TimestampType}
import org.joda.time.{DateTime, Days}
import redis.clients.jedis.Jedis

/*
Created By samathashetty on 13/12/18
*/
object RedisReset {

  def main(args: Array[String]): Unit = {
    val session = ConfigFactory.getSparkSession


    val configPath = args(0)


    val conf = new Configuration()
    val fileSystem = FileSystem.get(conf)
    implicit val formats = net.liftweb.json.DefaultFormats
    val path = new Path(configPath)
    val json = parse(scala.io.Source.fromInputStream(fileSystem.open(path)).mkString)
    val config = json.extract[Config]



    val credentials = config.credentialMap

    val batchReader: SparkReader = InputReaderFactory("batch")

    val date = DateTime.now()

    val srcpath = "s3n://myntra-results/funnelLookup/" + "y=" + date.getYear + "/m=" + "%02d".format(date.getMonthOfYear) + "/d=" + "%02d".format(date.getDayOfMonth) + "/*/"



      val funnelView = batchReader.readFiles(srcpath, "")
      .select("session_id", "ref_widget_item_whom", "ref_widget_whom", "data_set_value", "session_end_date", "is_logged_in", "os", "client_ts", "event_type", "uidx")
      .filter("ref_widget_item_whom is not null and data_set_value is not null and ref_widget_item_whom rlike '[2|19]:[0-9|a-z|A-Z|-]*'")
      .withColumn("timestamp", from_unixtime((col("client_ts") + 19800000) / 1000, "yyyy-MM-dd HH:mm:ss").cast(TimestampType))
      //        timeToBinTime(col("timestamp").cast(LongType), lit(5)).cast(StringType) as "bucket",

      .withColumn("bucket", timeToBinTime(col("timestamp").cast(StringType), lit(500)).cast(StringType))


    val rdd = funnelView.select(
      concat(col("uidx"), lit("%"), col("data_set_value")) as "key",
      concat(col("ref_widget_whom"), lit("%"), col("os"), lit("%"), col("is_logged_in"), lit("%"),
        col("bucket")) as "value"
    ).rdd.map(f => (f.getString(f.fieldIndex("key")), f.getString(f.fieldIndex("value"))))

    val redisConfig = RedisProp.getConfig(credentials("nrt_redis"))
    session.sparkContext.toRedisHASH(rdd, "eventsEarly")(redisConfig)

//    val jc = new Jedis
//    jc.del("*")

  }

}
