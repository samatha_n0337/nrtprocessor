package com.myntra.streaming.config

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/*
Created By samathashetty on 19/12/18
*/
object ConfigFactory {

  private var sparkSession: SparkSession = null

  /**
    * Initialize spark session instance
    * @param sConf SparkConf
    */
  def initSparkSession(sConf: SparkConf) {
    setSparkSession(sConf)
  }

  /**
    * Initialize spark session instance
    * @param sConf SparkConf
    */

  def setSparkSession(sConf: SparkConf) = {
    sparkSession = SparkSession
      .builder()
      .config(sConf)
      .config("spark.sql.autoBroadcastJoinThreshold", "-1")
      .config("spark.sql.crossJoin.enabled", "true")
      .config("spark.redis.host", "10.0.6.178")
      .config("spark.redis.port", "6379")
      .getOrCreate()
  }

  def getSparkSession: SparkSession={
    if(sparkSession == null) {

      initSparkSession(new SparkConf().setAppName(getClass.getName))
    }

    sparkSession
  }


}
