package com.myntra.streaming.config

import com.myntra.utils.EmptyClass

/*
Created By samathashetty on 19/12/18
*/

case class Config(credentialMap: Map[String, Credential], appName: String) extends EmptyClass

case class Credential(connectionName: String, dbName: String, username: String, password: String, url: String, port: String)

case class JdbcStream(checkpoint: String, url: String, dbTable: String, user: String, password: String) extends EmptyClass






