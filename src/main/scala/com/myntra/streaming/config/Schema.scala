package com.myntra.streaming.config

import org.apache.spark.sql.types._

/*
Created By samathashetty on 19/11/18
*/
object Schema {

  val schemaType = StructType(Array(
    StructField("topic", StringType, true),
    StructField("_env", StringType, true),
    StructField("eventType", StringType, true),
    StructField("payload", StringType, true)
  ))

  val secorSchema = StructType(Array(
    StructField("timestamp", TimestampType, true),
    StructField("eventType", StringType, true),
    StructField("payload", StringType, true)
  ))

  val evSchemaType = StructType(Array(
    StructField("ev", StructType(Array(
      StructField("event_type", StringType, true),
      StructField("_t", StringType, true),
      StructField("_ts", StringType, true),
      StructField("app", StructType(Array(
        StructField("name", StringType, true))), true),
      StructField("client_id", StringType, true),
      StructField("device", StructType(Array(
        StructField("category", StringType, true),
        StructField("device_id", StringType, true),
        StructField("os", StringType, true),
        StructField("height", LongType, true),
        StructField("width", LongType, true)
      )), true),
      StructField("event_meta_version", StringType, true),
      StructField("header_info", StructType(Array(
        StructField("forwarded_for_ip", StringType, true),
        StructField("source", StringType, true),
        StructField("user_agent", StringType, true))), true),
      StructField("payload", StructType(Array(
        StructField("widget", StructType(Array(
          StructField("data_set", StructType(Array(
            StructField("data", ArrayType(
              StructType(Array(
                StructField("entity_id", StringType),
                StructField("entity_optional_attributes", StructType(Array(
                  StructField("whom", StringType, true),
                  StructField("cardType", StringType, true)
                )), true)
              )), true)
              , true))), true))), true),
        StructField("screen", StructType(Array(
          StructField("data_set", StructType(Array(
            StructField("entity_optional_attributes", StructType(Array(
              StructField("sku-id", StringType, true)
            )), true),
            StructField("entity_id", StringType, true),
            StructField("entity_type", StringType, true))), true),
          StructField("name", StringType, true),
          StructField("type", StringType, true),
          StructField("url", StringType, true),
          StructField("variant", StringType, true))), true))), true),
      StructField("session", StructType(Array(
        StructField("is_first_session", StringType, true),
        StructField("landing_screen", StringType, true),
        StructField("session_id", StringType, true),
        StructField("session_referrer", StructType(Array(
          StructField("channel", StringType, true),
          StructField("medium", StringType, true),
          StructField("referrer", StringType, true),
          StructField("source", StringType, true))), true),
        StructField("start_time", LongType, true))), true),
      StructField("time_stamp", LongType, true),
      StructField("user", StructType(Array(
        StructField("customer_id", StringType, true),
        StructField("is_logged_in", BooleanType, true),
        StructField("uidx", StringType, true))), true)
    )), true)))



  val folSchema = StructType(Array(
    StructField("customer_login", StringType, true),
    StructField("order_created_date", StringType, true),
    StructField("order_created_time", StringType, true),
    StructField("device_channel", StringType, true),
    StructField("fact_user_agent", StringType, true),
    StructField("order_group_id", StringType, true),
    StructField("item_revenue", StringType, true),
    StructField("cashback_redeemed", StringType, true),
    StructField("gift_charge", StringType, true),
    StructField("shipping_charge", StringType, true),
    StructField("loyalty_used", StringType, true),
    StructField("mynts_used", StringType, true),
    StructField("brand", StringType, true),
    StructField("seller_id", StringType, true),
    StructField("sales_commission", StringType, true),
    StructField("logistics_charges", StringType, true),
    StructField("tax_amount", StringType, true),
    StructField("item_purchase_price_inc_tax", StringType, true),
    StructField("vendor_funding", StringType, true),
    StructField("royalty_commission", StringType, true),
    StructField("entry_tax", StringType, true),
    StructField("is_shipped", StringType, true),
    StructField("is_realised", StringType, true),
    StructField("item_quantity", StringType, true),
    StructField("sku_id", StringType, true),
    StructField("platform_id", StringType, true)
  ))

  val folBatchSchema = StructType(Array(
    StructField("order_created_date", LongType),
    StructField("order_id", StringType),
    StructField("order_group_id", StringType),
    StructField("idcustomer", LongType),
    StructField("style_id", StringType),
    StructField("R_BRAND", StringType),
    StructField("R_articlt_type", StringType),
    StructField("R_gender", StringType),
    StructField("item_revenue_inc_Cashback", DoubleType),
    StructField("quantity", IntegerType),
    StructField("shipping_charges", DoubleType),
    StructField("gift_charges", DecimalType.apply(12, 4)),
    StructField("tax", DoubleType),
    StructField("cogs", DecimalType.apply(22, 4)),
    StructField("royalty_commission", DoubleType),
    StructField("entry_tax", DoubleType),
    StructField("stn_input_vat_reversal", DoubleType),
    StructField("product_discount", DoubleType),
    StructField("coupon_discount", DoubleType),
    StructField("is_first_order", StringType),
    StructField("is_first_brand_order", StringType),
    StructField("customer_login", StringType),
    StructField("IS_LOG", StringType),
    StructField("session_end_date", StringType),
    StructField("uidx", StringType),
    StructField("data_set_value", StringType),
    StructField("ref_event_type", StringType),
    StructField("referrer_url", StringType),
    StructField("REF_card_type", StringType),
    StructField("ref_landing_screen", StringType),
    StructField("REF_List_Page", StringType),
    StructField("event_type", StringType),
    StructField("app_id_ev", IntegerType),
    StructField("ref_id_EV", StringType),
    StructField("ref_widget_item_whom", StringType),
    StructField("ref_widget_whom", StringType),
    StructField("ref_utm_source", StringType),
    StructField("ref_utm_campaign", StringType),
    StructField("ref_utm_medium", StringType),
    StructField("ref_notification_class", StringType),
    StructField("app_version", StringType),
    StructField("ms_engagement", StringType),
    StructField("ms_purhcase_performance", StringType),
    StructField("ms_discount_affinity", StringType),
    StructField("ms_customer_satisfaction", StringType),
    StructField("ms_purchase_intent", StringType),
    StructField("ref_similar_button_click", StringType),
    StructField("ref_widget_item_whom_lp", StringType),
    StructField("ref_widget_whom_lp", StringType),
    StructField("ref_widget_v_position_lp", StringType),
    StructField("ref_widget_item0_h_position_lp", StringType),
    StructField("ref_card_type_lp", StringType),
    StructField("ref_data_set_name_lp", StringType),
    StructField("ref_event_type_lp", StringType),
    StructField("look_widget_item_whom", StringType),
    StructField("look_widget_whom", StringType),
    StructField("ref_hp_screen_name", StringType),
    StructField("ref_publisher_tag", StringType),
    StructField("EV", StringType),
    StructField("LIST_PAGE_ATTRIBUTION", StringType),
    StructField("last_modified_on", StringType),
    StructField("URL_DO", StringType),
    StructField("IMG_URL_DO", StringType),
    StructField("content_type", StringType),
    StructField("ref_id", StringType),
    StructField("banner_date", LongType),
    StructField("campaign_id", StringType),
    StructField("campaign_url", StringType),
    StructField("banner_id", StringType),
    StructField("banner_url", StringType),
    StructField("campaign_title", StringType),
    StructField("campaign_breif", StringType),
    StructField("banner_status", StringType),
    StructField("landing_url", StringType),
    StructField("landing_image", StringType),
    StructField("associated_department", StringType),
    StructField("campaign_tags", StringType),
    StructField("article_type", StringType),
    StructField("category", StringType),
    StructField("brands", StringType),
    StructField("business_units", StringType),
    StructField("merchandise_types", StringType),
    StructField("visibility_slot", StringType),
    StructField("revenue_campaign", StringType),
    StructField("image_url", StringType),
    StructField("created_by", StringType),
    StructField("cip", StringType),
    StructField("REFERER_TYPE", StringType),
    StructField("Product_Interest", StringType),
    StructField("DIM_STYLE_master_category", StringType),
    StructField("DIM_STYLE_business_unit", StringType),
    StructField("DIM_STYLE_subcategory", StringType),
    StructField("curation_screen_name", StringType)
  ))

  val eventsSchema = StructType(Array(
    StructField("refId", StringType),
    StructField("event_type", StringType),
    StructField("count", LongType)
  ))
}
