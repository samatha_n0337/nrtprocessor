package com.myntra.streaming.model

/*
Created By samathashetty on 04/11/18
*/
case class FlashModel(refId: String, timestamp:Long, styleId: String, uidx: String)
