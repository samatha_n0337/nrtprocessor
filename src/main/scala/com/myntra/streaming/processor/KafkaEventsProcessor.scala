package com.myntra.streaming.processor

import java.text.SimpleDateFormat

import com.myntra.streaming.config.{ConfigFactory, Credential}
import com.myntra.streaming.config.Schema.{evSchemaType, schemaType}
import com.myntra.streaming.reader.{InputReaderFactory, SparkReader}
import com.myntra.utils.Constants
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, TimestampType}
import org.apache.spark.sql.{Dataset, Row}

/*
Created By samathashetty on 19/11/18
*/
object KafkaEventsProcessor {


  def calcStyleId(event_type: String, styleId: String, whom: String, sku_id: String, entityId: String): String = {
    val ret =
      if (styleId != null && styleId.matches(Constants.STYLE_ID_REGEX))
        styleId
      else if (entityId != null && entityId.matches(Constants.STYLE_ID_REGEX))
        entityId
      else if (whom != null && whom.matches(Constants.STYLE_ID_REGEX))
        whom
      else if (sku_id != null && sku_id.matches(Constants.STYLE_ID_REGEX))
        sku_id
      else
        null

    //    val retStyleId: String =
    //      if (List("feedCardClick", "streamCardClick", "feedCardLike", "streamCardLike", "feedCardLoad", "streamCardLoad", "feedCardShare", "streamCardShare",
    //        "feedCardUnlike", "streamCardUnlike", "streamCardChildClick", "feedCardChildClick", "streamCardChildLike", "feedCardChildLike", "streamCardChildLoad",
    //        "streamCardLoadNonUnique", "streamCardChildLoadNonUnique", "feedCardChildLoad", "feedCardChildLoadNonUnique", "feedCardLoadNonUnique", "streamCardChildShare",
    //        "feedCardChildShare").contains(event_type))
    //        whom
    //      else if ("addToCart".equalsIgnoreCase(event_type))
    //        sku_id
    //      //else if (List("AddToCollection", "collection-load", "CreateCollectionSuccess", "collection-click", "collection-load", "createCollectionInitiated", "share").contains(event_type))
    //      //  entityId
    //      else entityId
    //
    //    if(retStyleId != null && retStyleId.matches("[0-9]*"))
    //      return retStyleId
    //    null


    ret
  }

  val style_id = udf((event_type: String, styleId: String, whom: String, sku_id: String, entityId: String) => calcStyleId(event_type: String, styleId: String, whom: String, sku_id: String, entityId: String))


  def init(credential: Credential): Dataset[Row] = {

    val kafkaReader: SparkReader = InputReaderFactory("kafka")

    println("kafkaConn:" + credential.dbName + ":::" + credential.url)
    val kafkaDf = kafkaReader.readFiles(credential.url + ":" + credential.port, credential.dbName)


    val eventsStream = kafkaDf
      .selectExpr("cast (key as string) AS key", "cast (value as string) AS value")
      .withColumn("valueJson", from_json(col("value"), schemaType))
      .withColumn("payload", from_json(col("valueJson.payload"), evSchemaType))


    val eventsFiltered = eventsStream.select(
      col("payload.ev.session.session_id") as "session_id",
      col("payload.ev.app.name") as "app_name",
      col("payload.ev.device.os") as "os",
      coalesce(col("payload.ev._t"), col("payload.ev.event_type")) as "event_type",
      col("payload.ev.payload.screen.data_set.entity_id") as "styleId",
      col("payload.ev.payload.screen.data_set.entity_optional_attributes.sku-id") as "sku_id",
      col("payload.ev.user.uidx") as "uidx",
      col("payload.ev.user.is_logged_in") as "is_logged_in",
      col("payload.ev.time_stamp") as "timestamp",
      explode(col("payload.ev.payload.widget.data_set.data")) as "data"
    )
      .filter("event_type in ('banner-child-click', 'banner-click', 'navi-child-click', 'navi-click', 'CardClick', 'cardtype-click', 'LeftBurgerMenuOpen'," +
        "'LookBookViewed', 'LookProductClicked', 'LookProductViewed', 'LookSaved', 'LookWidgetLoad', 'moveToCart', 'SearchScreenLoad', 'list_page_exit_event', 'Similar products load'," +
        "'Similar products loaded', 'SimilarClick', 'similarProductClick', 'Product list loaded', 'CortexClick', 'addToList', 'product_click_list_page', 'addToCart', 'AddToCollection'," +
        "'App Launch', 'appLaunch', 'collection-click', 'collection-product-click', 'collection-load', 'feedCardChildClick', 'feedCardClick', 'Level1LeftNavClick', 'level1-nav-click'," +
        "'Level2LeftNavClick', 'level2-nav-click', 'Level3LeftNavClick', 'level3-nav-click', 'listLoad', 'RightBurgerMenuOpen', 'RightNavChildClick', 'ScreenLoad', 'screen-load'," +
        "'searchFired', 'SearchFired', 'searchListLoad', 'streamCardChildClick', 'streamCardClick', 'topNavClick', 'Similar products button clicked', 'deep - link', 'DeepLink'," +
        "'push-notification', 'streamCardChildLoad', 'streamCardChildLoadNonUnique', 'feedCardChildLoad', 'feedCardChildLoadNonUnique','streamCardLoad', 'streamCardLoadNonUnique', " +
        "'feedCardLoad', 'feedCardLoadNonUnique')")
      .filter("os = 'Android'")
      .filter("app_name = 'MyntraRetailAndroid'")


    val eventsFil = eventsFiltered
      .select(
        col("session_id"),
        col("app_name"),
        col("os"),
        col("event_type"),
        col("styleId"),
        col("uidx"),
        col("is_logged_in"),
        from_unixtime((col("timestamp") + 19800000) / 1000, "yyyy-MM-dd HH:mm:ss").cast(TimestampType) as "timestamp",
        when(col("event_type").isin("streamCardClick", "feed-card-child-click", "stream-card-child-click",
          "feedCardChildClick", "feedCardClick", "streamCardChildClick"), lit(1)).otherwise(lit(0)) as "click",
        col("data.entity_id") as "entity_id",
        when(col("event_type").isin("streamCardChildLoad", "streamCardChildLoadNonUnique", "feedCardChildLoad", "feedCardChildLoadNonUnique",
          "streamCardLoad", "streamCardLoadNonUnique", "feedCardLoad", "feedCardLoadNonUnique").and(col("data.entity_optional_attributes.cardType").isin("banner", "BANNER", "event-banner")), lit(1))
          .otherwise(lit(0)) as "Impressions",
        style_id(col("event_type"), col("styleId"), col("data.entity_optional_attributes.whom"), col("sku_id"),
          col("data.entity_id")) as "style_id"
      ).withColumn("bucket", timeToBinTime(col("timestamp").cast(StringType), lit(500)).cast(StringType))

    eventsFil
  }

  def roundTotheNearestBin(timestamp: String, bin: Int): Long = {
    val initialFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val finalFormat = new SimpleDateFormat("yyyyMMddHHmmss")
    val dateTimeFormatted: Long = finalFormat.format(initialFormat.parse(timestamp)).toLong
    dateTimeFormatted - (dateTimeFormatted % (bin))
  }


  val timeToBinTime = udf((timestamp: String, bin: Int) => roundTotheNearestBin(timestamp, bin))


  val appIdFromRef = udf((refId: String) => if (null == refId || refId.indexOf(":") == -1) null
  else {
    refId.substring(0, refId.indexOf(":"))
  })

}
