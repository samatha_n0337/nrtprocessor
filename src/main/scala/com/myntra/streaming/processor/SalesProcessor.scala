package com.myntra.streaming.processor

import java.text.SimpleDateFormat
import java.util
import java.util.Calendar

import com.myntra.streaming.application.BatchApplication.dateChangeFormat
import com.myntra.streaming.application.MainApplication
import com.myntra.streaming.reader.{InputReaderFactory, SparkReader}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, TimestampType}
import org.apache.spark.sql.{Dataset, Row, functions}
import com.myntra.utils.Utils._
import org.joda.time.DateTime

/*
Created By samathashetty on 19/11/18
*/
object SalesProcessor {



  def init(): Dataset[Row] = {

    val sparkStreamReader: SparkReader = InputReaderFactory("amazonS3")
    val batchReader: SparkReader = InputReaderFactory("batch")

    val today = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime)
    val folDF = sparkStreamReader.readFiles("fol", "temp_fol")
      .select(
        col("customer_login") as "uidx",
        coalesce(col("item_revenue"), lit(0.0)).cast(DoubleType) as "revenue",
        col("sku_id"),
        functions.unix_timestamp(col("order_created_date"), "yyyyMMdd").cast(TimestampType) as "order_created_at",
        col("is_realised"),
        col("is_shipped"),
        col("platform_id"),
        col("order_created_date")
      )
      .filter("uidx not like 'sfloadtest-%'")
      .filter("is_realised = 1 or is_shipped=1")
      .filter("order_created_date = " + today)
//      .groupBy("uidx", "sku_id", "platform_id").agg(sum("revenue") as "revenue")


    val dimProduct = batchReader.readFiles("s3n://madlytics/udpAggregates/sources/bidb.dim_product/", "")
      .select(col("sku_id"), col("style_id") as "styleId")


    val folStyleId = folDF.join(dimProduct, "sku_id")
      .withColumn("dimensionFields", MainApplication.fillEvents(col("uidx"), col("styleId")))




    folStyleId

  }
}
