package com.myntra.streaming.processor

import java.text.SimpleDateFormat

import com.myntra.streaming.config.Schema.evSchemaType
import com.myntra.streaming.reader.{InputReaderFactory, SparkReader}
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DoubleType, LongType, TimestampType}

/*
Created By samathashetty on 21/11/18
*/
object SecorEventsProcessor {


  def init(): Dataset[Row] = {

    val sparkStreamReader: SparkReader = InputReaderFactory("amazonS3")

    val folDF = sparkStreamReader.readFiles("secor","temp_fol")
      .withColumn("payloadJson", from_json(col("payload"), evSchemaType))

    folDF.printSchema()
    
    val eventsFiltered = folDF.select(
      col("payloadJson.session.session_id") as "session_id",
      col("payloadJson.app.name") as "app_name",
      col("payloadJson.device.os") as "os",
      col("payloadJson.event_type") as "event_type",
      col("payloadJson.payload.screen.data_set.entity_id") as "styleId",
      col("payloadJson.user.uidx") as "uidx",
      col("payloadJson.user.is_logged_in") as "is_logged_in",
      col("payloadJson.time_stamp").cast(TimestampType) as "timestamp",
      col("payloadJson.time_stamp").cast(LongType) as "timestampLong",
      explode(col("payloadJson.payload.widget.data_set.data")) as "data"
    )
//      .filter(col("event_type").isin("banner-child-click", "banner-click", "navi-child-click", "navi-click", "CardClick", "cardtype-click", "LeftBurgerMenuOpen",
//      "LookBookViewed", "LookProductClicked", "LookProductViewed", "LookSaved", "LookWidgetLoad", "moveToCart", "SearchScreenLoad", "list_page_exit_event", "Similar products load",
//      "Similar products loaded", "SimilarClick", "similarProductClick", "Product list loaded", "CortexClick", "addToList", "product_click_list_page", "addToCart", "AddToCollection",
//      "App Launch", "appLaunch", "collection-click", "collection-product-click", "collection-load", "feedCardChildClick", "feedCardClick", "Level1LeftNavClick", "level1-nav-click",
//      "Level2LeftNavClick", "level2-nav-click", "Level3LeftNavClick", "level3-nav-click", "listLoad", "RightBurgerMenuOpen", "RightNavChildClick", "ScreenLoad", "screen-load",
//      "searchFired", "SearchFired", "searchListLoad", "streamCardChildClick", "streamCardClick", "topNavClick", "Similar products button clicked", "deep - link", "DeepLink",
//      "push-notification"))
//      .filter(col("os").equalTo(lit("Android")))
//      .filter(col("app_name").equalTo(lit("MyntraRetailAndroid")))


    val eventsFil = eventsFiltered
      .select(
        col("session_id"),
        col("app_name"),
        col("os"),
        col("event_type"),
        col("styleId"),
        col("uidx"),
        col("is_logged_in"),
        col("timestamp"),
        timeToBinTime(col("timestamp").cast(LongType), lit(5)) as "bucket",
        when(col("event_type").isin("streamCardClick", "feed-card-child-click", "stream-card-child-click",
          "feedCardChildClick", "feedCardClick", "streamCardChildClick"), lit(1)).otherwise(lit(0)) as "click",
        col("data.entity_id") as "entity_id",
        when(col("data.entity_optional_attributes.cardType").isin("banner", "BANNER", "event-banner"), lit(1)) as "Impressions"
      )


    val eventsWithRefId = eventsFil.
      groupBy(
        window(col("timestamp"), "10 minutes", "5 minutes"),
        col("session_id"), col("is_logged_in")
      ).agg(
      sum("click") as "clicks",
      sum("Impressions") as "impressions",
      first("entity_id", ignoreNulls = true) as "refId",
      lit(0.0) as "revenue",
      first("bucket", true) as "bucket",
      first("uidx") as "uidx",
      first("os", true) as "os",
      collect_list("styleId") as "styleId_list"
    ).withColumn("appId", appIdFromRef(col("refId")))
//      .filter("appId in ('4', '19')")


    eventsWithRefId
  }


  def roundTotheNearestBin(timestamp: Long, bin: Int): Long = {
    val sdf = new SimpleDateFormat("yyyyMMddHHmm")
    val dateTimeFormatted: Long = sdf.format(timestamp * 1000).toLong
    dateTimeFormatted - (dateTimeFormatted % bin)
  }


  val timeToBinTime = udf((timestamp: Long, bin: Int) => roundTotheNearestBin(timestamp, bin))


  val appIdFromRef = udf((refId: String) => if (null == refId || refId.indexOf(":") == -1) null
  else {
    refId.substring(0, refId.indexOf(":"))
  })


}
