package com.myntra.streaming.reader

import com.myntra.streaming.config.{ConfigFactory, Schema}
import org.apache.spark.sql.{Dataset, Row}

/*
Created By samathashetty on 22/10/18
*/
class AmazonS3Reader extends SparkReader {

  var myAccessKey: String = null
  var mySecretKey: String = null

  def initCredentials() = {

    //TODO : should be passed from config
    myAccessKey = ""
    mySecretKey = ""


  }


  def map = Map(

    "fol" -> ("s3n://myntra-temp/fact_order_live/", "\u0001", Schema.folSchema, "csv"),
    "secor" -> ("s3://madlytics/rawevents/madlytics_1/y=2018/m=12/d=30/h=09/*/type=default", null, Schema.secorSchema, "orc")

  )

  override def readFiles(srcPath: String, tableName: String): Dataset[Row] = {


    val valuesTuple = map(srcPath)
    val session = ConfigFactory.getSparkSession
    val sc = session.sparkContext
    val hadoopConf = sc.hadoopConfiguration
    //TODO: red from config
    hadoopConf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
    hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAJSP4DMU3X2BNOLRA")
    hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")


    val df = session.readStream
      .option("sep", valuesTuple._2)
      .schema(valuesTuple._3).format(valuesTuple._4)
      .load(valuesTuple._1)

    df

  }
}
