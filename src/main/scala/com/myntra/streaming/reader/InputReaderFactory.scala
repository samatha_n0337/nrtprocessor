package com.myntra.streaming.reader

/*
Created By samathashetty on 22/10/18
*/
object InputReaderFactory {

  def apply(inputType: String): SparkReader = {
    inputType match {
      case "kafka" => new KafkaReader
      case "amazonS3" => new AmazonS3Reader
      case "batch" => new BatchReader
      case "redis" => new RedisReader
    }
  }
}