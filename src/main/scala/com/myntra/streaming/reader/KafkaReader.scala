package com.myntra.streaming.reader

import org.apache.spark.sql.{Dataset, Row}
import com.myntra.streaming.config._
/*
Created By samathashetty on 19/10/18
*/
class KafkaReader extends SparkReader {


  override def readFiles(srcPath: String, tableName: String): Dataset[Row] = {

    val session = ConfigFactory.getSparkSession

    val df =
      session.
        readStream.
        format("kafka").
        option("kafka.bootstrap.servers", srcPath).
        option("startingOffsets", "latest").
        option("subscribe",tableName).
        option("failOnDataLoss", "false").
        option("session.timeout.ms", "150000").
        option("heartbeat.interval.ms", "50000").
        option("request.timeout.ms", "180000").
        option("kafkaConsumer.pollTimeoutMs", "150000" ).
        option("maxOffsetsPerTrigger", "1000" ).
        load()


    df
  }

}
