package com.myntra.streaming.reader

import com.myntra.streaming.RedisProp
import com.myntra.streaming.config.ConfigFactory
import org.apache.spark.sql.functions.{coalesce, col, lit}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Dataset, Row}

/*
Created By samathashetty on 13/11/18
*/
class RedisReader extends SparkReader {


  override def readFiles(srcPath: String, tableName: String): Dataset[Row] = {

    val session = ConfigFactory.getSparkSession
    val redisConfig = RedisProp.getConfig(null)

    val ds = session.read
      .format("org.apache.spark.sql.redis")
      .schema(
        StructType(Array(
          StructField("uidx", StringType),
          StructField("styleId", StringType),
          StructField("refId", StringType),
          StructField("clicks", StringType),
          StructField("impressions", StringType),
          StructField("os", StringType),
          StructField("is_logged_in", StringType),
          StructField("revenue", DoubleType)
        )
        )
      )
      .option("keys.pattern", "clicksImpression:*")
      .option("key.column", "refId")
      .load()

    ds.select(
      col("refId"),
      col("os"),
      col("is_logged_in").cast(BooleanType) as "is_logged_in",
      col("clicks").cast(LongType) as "clicks",
      col("impressions").cast(LongType) as "impressions",
      coalesce(col("revenue"), lit(0.0)) as "revenue"
    )

//    val userRDD  = rdd1.map(f=>Row(f._2))
//    val structFields = new util.ArrayList[StructField]
//    structFields.add(DataTypes.createStructField( "uidx", DataTypes.StringType, true ));
//    structFields.add(DataTypes.createStructField( "styleId", DataTypes.StringType, false ));
//    structFields.add(DataTypes.createStructField( "refId", DataTypes.StringType, false ));
//    structFields.add(DataTypes.createStructField( "clicks", DataTypes.LongType, false ));
//    structFields.add(DataTypes.createStructField( "impressions", DataTypes.LongType, false ));
//    structFields.add(DataTypes.createStructField( "os", DataTypes.StringType, false ));
//    structFields.add(DataTypes.createStructField( "is_logged_in", DataTypes.BooleanType, false ));
//    val structType = DataTypes.createStructType(structFields);
//
//    val ds = session.createDataFrame(userRDD, structType);



  }


}
