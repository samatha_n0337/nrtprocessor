package com.myntra.streaming.reader

import org.apache.spark.sql.{Dataset, Row}

/*
Created By samathashetty on 22/10/18
*/
abstract class SparkReader {

  def readFiles(srcPath: String, tableName: String): Dataset[Row]

}
