package com.myntra.streaming.sink

/*
Created By samathashetty on 30/11/18
*/


import java.sql.Connection

import com.myntra.utils.Utils
import org.apache.spark.internal.Logging
import org.apache.spark.sql._
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.execution.datasources.jdbc.JdbcUtils._
import org.apache.spark.sql.execution.datasources.jdbc._
import org.apache.spark.sql.execution.streaming.Sink
import org.apache.spark.sql.jdbc.JdbcDialects
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types._

//Reference at https://github.com/GaalDornick/spark-jdbc/blob/master/sql/core/src/main/scala/org/apache/spark/sql/execution/streaming/JdbcSink.scala

case class JDBCSink(
                     sqlContext: SQLContext,
                     parameters: Map[String, String],
                     partitionColumns: Seq[String],
                     outputMode: OutputMode) extends Sink with Logging {
  val options = new JDBCOptions(parameters)
  val sinkLog = new JDBCSinkLog(parameters, sqlContext.sparkSession)
  // If user specifies a batchIdCol in the parameters, then it means that the user wants exactly
  // once semantics. This column will store the batch Id for the row when an uncommitted batch
  // is replayed, JDBC SInk will delete the rows that were added to the previous play of the
  // batch
  val batchIdCol = parameters.get("batchIdCol")
  val revenueCalc = parameters.get("revenueCalc")

  def addBatch(batchId: Long, df: DataFrame): Unit = {

    val schema: StructType = batchIdCol.map(colName => df.schema.add(colName, LongType, false))
      .getOrElse(df.schema)
    val conn = JdbcUtils.createConnectionFactory(options)()
    try {
      if ((revenueCalc == null || revenueCalc == "false") && sinkLog.isBatchCommitted(batchId, conn)) {
        logInfo(s"Skipping already committed batch $batchId")
      } else {
        sinkLog.startBatch(batchId, conn)
        val isCaseSensitive = true //sqlContext.conf.caseSensitiveAnalysis

        val tableExists = JdbcUtils.tableExists(conn, options)
        if (tableExists) {
          if (outputMode == OutputMode.Complete()) {

            if (options.isTruncate && isCascadingTruncateTable(options.url).contains(false)) {
              // In this case, we should truncate table and then load.
              truncateTable(conn, options)
              saveRows(df, isCaseSensitive, options, batchId)
            } else {
              // Otherwise, do not truncate the table, instead drop and recreate it
              dropTable(conn, options.table)
              createTable(conn, df, options)
              saveRows(df, isCaseSensitive, options, batchId)
            }
          } else if (outputMode == OutputMode.Append() || outputMode == OutputMode.Update()) {
            saveRows(df, isCaseSensitive, options, batchId)
          } else {
            throw new IllegalArgumentException(s"$outputMode not supported")
          }
        } else {
          createTable(conn, df, options)
          saveRows(df, isCaseSensitive, options, batchId)
        }
        sinkLog.commitBatch(batchId, conn)

      }
    } finally {
      conn.close()
      Utils.instance().populateRedisMap()

      //TODO: call the redis publish here
    }
  }

  /**
    * Saves the RDD to the database in a single transaction.
    */
  def saveRows(
                df: DataFrame,
                isCaseSensitive: Boolean,
                options: JDBCOptions,
                batchId: Long): Unit = {
    val url = options.url
    val table = options.table
    val dialect = JdbcDialects.get(url)
    val getConnection: () => Connection = createConnectionFactory(options)
    val batchSize = options.batchSize
    val isolationLevel = options.isolationLevel

    val repartitionedDF = options.numPartitions match {
      case Some(n) if n <= 0 => throw new IllegalArgumentException(
        s"Invalid value `$n` for parameter `${JDBCOptions.JDBC_NUM_PARTITIONS}` in table writing " +
          "via JDBC. The minimum value is 1.")
      case Some(n) if n < df.rdd.getNumPartitions => df.coalesce(n)
      case _ => df
    }
    if (batchIdCol.isEmpty) {

      val insertStmt = getInsertStatement(table, df.schema, None, isCaseSensitive, dialect)
      val rddSchema = df.schema
      repartitionedDF.queryExecution.toRdd.foreachPartition(iterator => {
        StructuredJdbcUtils.saveInternalPartition(
          getConnection, table, iterator, rddSchema, insertStmt, batchSize, dialect, isolationLevel)
      })
    } else {

      // batchId col is defined.. construct a schema by adding the batchId col to the DF schema
      // also put the value of the batch id to the end of every row in the DF
      val dfSchema = df.schema
      val rddSchema: StructType = df.schema.add(batchIdCol.get, LongType, false)
      val insertStmt = getInsertStatement(table, rddSchema, None, isCaseSensitive, dialect)
      repartitionedDF.queryExecution.toRdd.foreachPartition(iterator => {
        StructuredJdbcUtils.saveInternalPartition(
          getConnection, table
          , iterator.map(ir => InternalRow.fromSeq(ir.toSeq(dfSchema) :+ batchId))
          , rddSchema, insertStmt, batchSize, dialect, isolationLevel)
      })
    }
  }


  def saveMode(outputMode: OutputMode): SaveMode = {
    if (outputMode == OutputMode.Append() || outputMode == OutputMode.Update()) {
      SaveMode.Append
    } else if (outputMode == OutputMode.Complete()) {
      SaveMode.Append
    } else {
      throw new IllegalArgumentException(s"Output mode $outputMode is not supported by JdbcSink")
    }
  }
}