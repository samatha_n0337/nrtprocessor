package com.myntra.streaming.sink

import com.myntra.utils.Constants
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.execution.streaming.Sink
import org.apache.spark.sql.sources.{DataSourceRegister, StreamSinkProvider}
import org.apache.spark.sql.streaming.OutputMode

/*
Created By samathashetty on 01/12/18
*/

class JDBCSinkProvider extends StreamSinkProvider with DataSourceRegister {

  override def createSink(
                           sqlContext: SQLContext,
                           parameters: Map[String, String],
                           partitionColumns: Seq[String],
                           outputMode: OutputMode): Sink = {

    JDBCSink(sqlContext, parameters, partitionColumns, outputMode)
  }

  override def shortName(): String = Constants.STREAM_JDBC


}
