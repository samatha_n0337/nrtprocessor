//package com.myntra.streaming.sink
//
//import com.myntra.utils.Utils
//import org.apache.spark.internal.Logging
//import org.apache.spark.sql.{DataFrame, SQLContext}
//import org.apache.spark.sql.execution.streaming.Sink
//import org.apache.spark.sql.streaming.OutputMode
//import redis.clients.jedis.Jedis
//
///*
//Created By samathashetty on 21/12/18
//*/
//case class RedisSink(
//                     sqlContext: SQLContext,
//                     parameters: Map[String, String],
//                     partitionColumns: Seq[String],
//                     outputMode: OutputMode) extends Sink with Logging {
//
//
//  val tableName = parameters.getOrElse("table_name", null)
//  val keyCol = parameters.getOrElse("key_column", null)
//
//  val redisKey = parameters.getOrElse("redis_key", null)
//
//  override def addBatch(batchId: Long, data: DataFrame): Unit = {
//
//    data.queryExecution.toRdd.map(f => (f.getString(0), f.getString(1)))
//    .foreachPartition(
//      line => {
//
//        val jedis: Jedis = new Jedis("10.0.6.178");
//        jedis.hmset()
//        jedis.close();
//    )
//
////      .format("org.apache.spark.sql.redis")
////      .option("table", redisKey)
////      .option("key.column", keyCol)
////      .save()
//
//
//
//  }
//}
