//package com.myntra.streaming.sink

/*
Created By samathashetty on 01/12/18
*/
package org.apache.spark.sql.execution.streaming.sources

import com.myntra.utils.DBUtils
import org.apache.spark.api.python.PythonException
import org.apache.spark.sql._
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.execution.streaming.Sink
import org.apache.spark.sql.streaming.DataStreamWriter
//TODO: Use ForEachBatchSink from spark2.4

class StructuredForEachBatchSink[T](batchWriter: (Dataset[Row], Long) => Unit, encoder: ExpressionEncoder[Row])
  extends Sink {
  override def addBatch(batchId: Long, data: DataFrame): Unit = {
    val resolvedEncoder = encoder.resolveAndBind(
      data.logicalPlan.output,
      data.sparkSession.sessionState.analyzer)
    val rdd = data.queryExecution.toRdd.map[Row](resolvedEncoder.fromRow)(encoder.clsTag)
    val ds = data.sparkSession.createDataset(rdd)(encoder)

    DBUtils.writeToMemSql(ds, SaveMode.Overwrite, "nrt_admetrics4")
  }

  override def toString(): String = "foreachb"
}
