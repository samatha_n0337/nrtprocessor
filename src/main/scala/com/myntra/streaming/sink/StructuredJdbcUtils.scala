package org.apache.spark.sql

import java.sql.{Connection, PreparedStatement, SQLException}

import org.apache.spark.internal.Logging
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.apache.spark.sql.catalyst.util.CaseInsensitiveMap
import org.apache.spark.sql.execution.datasources.jdbc.{JDBCOptions, JdbcUtils}
import org.apache.spark.sql.jdbc.{JdbcDialect, JdbcDialects, JdbcType}
import org.apache.spark.sql.types._
import org.apache.spark.sql.util.SchemaUtils

import scala.util.control.NonFatal

/*
Created By samathashetty on 30/11/18
*/
object StructuredJdbcUtils extends Logging {

  def getJdbcType(dt: DataType, dialect: JdbcDialect): JdbcType = {
    dialect.getJDBCType(dt).orElse(JdbcUtils.getCommonJDBCType(dt)).getOrElse(
      throw new IllegalArgumentException(s"Can't get JDBC type for ${dt.simpleString}"))

  }

  // A `JDBCValueSetter` is responsible for setting a value from `Row` into a field for
  // `PreparedStatement`. The last argument `Int` means the index for the value to be set
  // in the SQL statement and also used for the value in `Row`.
  private type JDBCInternalValueSetter = (PreparedStatement, InternalRow, Int) => Unit

  def makeInternalSetter(
                          conn: Connection,
                          dialect: JdbcDialect,
                          dataType: DataType): JDBCInternalValueSetter = dataType match {
    case IntegerType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setInt(pos + 1, row.getInt(pos))
    case LongType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setLong(pos + 1, row.getLong(pos))
    case DoubleType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setDouble(pos + 1, row.getDouble(pos))
    case FloatType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setFloat(pos + 1, row.getFloat(pos))
    case ShortType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setInt(pos + 1, row.getShort(pos))
    case ByteType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setInt(pos + 1, row.getByte(pos))
    case BooleanType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setBoolean(pos + 1, row.getBoolean(pos))
    case StringType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setString(pos + 1, row.getString(pos))
    case BinaryType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setBytes(pos + 1, row.getBinary(pos))
    case TimestampType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setTimestamp(pos + 1, row.get(pos, dataType).asInstanceOf[java.sql.Timestamp])
    case DateType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setDate(pos + 1, row.get(pos, dataType).asInstanceOf[java.sql.Date])
    case t: DecimalType =>
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        stmt.setBigDecimal(pos + 1, row.getDecimal(pos, 20, 4).toJavaBigDecimal)
    case ArrayType(et, _) =>
      // remove type length parameters from end of type name
      val typeName = getJdbcType(et, dialect).databaseTypeDefinition
        .toLowerCase.split("\\(")(0)
      (stmt: PreparedStatement, row: InternalRow, pos: Int) =>
        val array = conn.createArrayOf(
          typeName,
          row.getArray(pos).toObjectArray(et))
        stmt.setArray(pos + 1, array)
    case _ =>
      (_: PreparedStatement, _: InternalRow, pos: Int) =>
        throw new IllegalArgumentException(
          s"Can't translate non-null value for field $pos")
  }

  /**
    * Saves a partition of a DataFrame to the JDBC database.  This is done in
    * a single database transaction (unless isolation level is "NONE")
    * in order to avoid repeatedly inserting data as much as possible.
    *
    * It is still theoretically possible for rows in a DataFrame to be
    * inserted into the database more than once if a stage somehow fails after
    * the commit occurs but before the stage can return successfully.
    *
    * This is not a closure inside saveTable() because apparently cosmetic
    * implementation changes elsewhere might easily render such a closure
    * non-Serializable.  Instead, we explicitly close over all variables that
    * are used.
    */
  def saveInternalPartition(
                             getConnection: () => Connection,
                             table: String,
                             iterator: Iterator[InternalRow],
                             rddSchema: StructType,
                             insertStmt: String,
                             batchSize: Int,
                             dialect: JdbcDialect,
                             isolationLevel: Int): Iterator[Byte] = {
    val conn = getConnection()
    var committed = false
    var finalIsolationLevel = Connection.TRANSACTION_NONE
    val insertSt = insertStmt// + " on duplicate key update clicks = clicks+ VALUES(clicks), impressions = impressions + VALUES(impressions), revenue = revenue + VALUES(revenue) "
    if (isolationLevel != Connection.TRANSACTION_NONE) {
      try {
        val metadata = conn.getMetaData
        if (metadata.supportsTransactions()) {
          // Update to at least use the default isolation, if any transaction level
          // has been chosen and transactions are supported
          val defaultIsolation = metadata.getDefaultTransactionIsolation
          finalIsolationLevel = defaultIsolation
          if (metadata.supportsTransactionIsolationLevel(isolationLevel)) {
            // Finally update to actually requested level if possible
            finalIsolationLevel = isolationLevel
          } else {
            println(s"Requested isolation level $isolationLevel is not supported; " +
              s"falling back to default isolation level $defaultIsolation")
          }
        } else {
          println(s"Requested isolation level $isolationLevel, but transactions are unsupported")
        }
      } catch {
        case NonFatal(e) => println("Exception while detecting transaction support", e)
      }
    }
    val supportsTransactions = finalIsolationLevel != Connection.TRANSACTION_NONE
    try {
      if (supportsTransactions) {
        conn.setAutoCommit(false) // Everything in the same db transaction.
        conn.setTransactionIsolation(finalIsolationLevel)
      }
      val stmt = conn.prepareStatement(insertSt)
      println("insertStmt:" + insertSt)
      val setters = rddSchema.fields.map(f => makeInternalSetter(conn, dialect, f.dataType))
      val nullTypes = rddSchema.fields.map(f => getJdbcType(f.dataType, dialect).jdbcNullType)
      val numFields = rddSchema.fields.length
      try {
        var rowCount = 0
        while (iterator.hasNext) {
          val row = iterator.next()
          var i = 0
          while (i < numFields) {
            if (row.isNullAt(i)) {
              stmt.setNull(i + 1, nullTypes(i))
            } else {
              setters(i).apply(stmt, row, i)
            }
            i = i + 1
          }
          stmt.addBatch()
          rowCount += 1
          if (rowCount % batchSize == 0) {
            stmt.executeBatch()
            rowCount = 0
          }
        }
        if (rowCount > 0) {
          stmt.executeBatch()
        }
      } finally {
        stmt.close()
      }
      if (supportsTransactions) {
        conn.commit()
      }
      committed = true
      Iterator.empty
    } catch {
      case e: SQLException =>

        val cause = e.getNextException
        println("exception:")
        e.printStackTrace()
        if (cause != null && e.getCause != cause) {
          if (e.getCause == null) {
            e.initCause(cause)
          } else {
            e.addSuppressed(cause)
          }
        }
        throw e
    } finally {
      if (!committed) {
        println("no commit")
        // The stage must fail.  We got here through an exception path, so
        // let the exception through unless rollback() or close() want to
        // tell the user about another problem.
        if (supportsTransactions) {
          conn.rollback()
        }
        conn.close()
      } else {
        // The stage must succeed.  We cannot propagate any exception close() might throw.
        try {
          conn.close()
        } catch {
          case e: Exception => println("Transaction succeeded, but closing failed", e)
        }
      }
    }
  }


  def createTable(
                   conn: Connection,
                   schema: StructType, sparkSession: SparkSession,
                   options: JDBCOptions): Unit = {
    val strSchema = schemaString(
      schema, sparkSession, options.url, options.createTableColumnTypes)
    val table = options.table
    val createTableOptions = options.createTableOptions
    // Create the table if the table does not exist.
    // To allow certain options to append when create a new table, which can be
    // table_options or partition_options.
    // E.g., "CREATE TABLE t (name string) ENGINE=InnoDB DEFAULT CHARSET=utf8"
    val sql = s"CREATE TABLE $table ($strSchema) $createTableOptions"
    println("sql" + sql)
    val statement = conn.createStatement
    try {
      statement.executeUpdate(sql)
    } finally {
      statement.close()
    }
  }

//  def updateTable(getConnection: () => Connection,
//                  table: String,
//                  iterator: Iterator[InternalRow],
//                  rddSchema: StructType,
//                  insertStmt: String,
//                  batchSize: Int,
//                  dialect: JdbcDialect,
//                  isolationLevel: Int): Iterator[Byte]= {
//    // Create the table if the table does not exist.
//    // To allow certain options to append when create a new table, which can be
//    // table_options or partition_options.
//    // E.g., "CREATE TABLE t (name string) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//    val sql = s"UPDATE TABLE $table SET revenue = ? where refId =? and os =? and is_logged_in = ? and bucket = ?"
//
//    val conn = getConnection()
//
//    println ("sql" + sql)
//    val statement = conn.createStatement
//    try {
////    statement.executeUpdate (sql)
//  } finally {
//    statement.close ()
//  }
//  }


  def schemaString(schema: StructType, sparkSession: SparkSession,
                   url: String,
                   createTableColumnTypes: Option[String] = None): String = {
    val sb = new StringBuilder()
    val dialect = JdbcDialects.get(url)
    val userSpecifiedColTypesMap = createTableColumnTypes
      .map(parseUserSpecifiedCreateTableColumnTypes(schema, sparkSession, _))
      .getOrElse(Map.empty[String, String])
    schema.fields.foreach { field =>
      val name = dialect.quoteIdentifier(field.name)
      val typ = userSpecifiedColTypesMap
        .getOrElse(field.name, getJdbcType(field.dataType, dialect).databaseTypeDefinition)
      val nullable = if (field.nullable) "" else "NOT NULL"
      sb.append(s", $name $typ $nullable")
    }
    if (sb.length < 2) "" else sb.substring(2)
  }

  private def parseUserSpecifiedCreateTableColumnTypes(
                                                        schema: StructType, sparkSession: SparkSession,
                                                        createTableColumnTypes: String): Map[String, String] = {
    def typeName(f: StructField): String = {
      // char/varchar gets translated to string type. Real data type specified by the user
      // is available in the field metadata as HIVE_TYPE_STRING
      if (f.metadata.contains(HIVE_TYPE_STRING)) {
        f.metadata.getString(HIVE_TYPE_STRING)
      } else {
        f.dataType.catalogString
      }
    }

    val userSchema = CatalystSqlParser.parseTableSchema(createTableColumnTypes)
    val nameEquality = sparkSession.sessionState.conf.resolver

    // checks duplicate columns in the user specified column types.
    userSchema.fieldNames.foreach { col =>
      val duplicatesCols = userSchema.fieldNames.filter(nameEquality(_, col))
      if (duplicatesCols.size >= 2) {
        throw new AnalysisException(
          "Found duplicate column(s) in createTableColumnTypes option value: " +
            duplicatesCols.mkString(", "))
      }
    }
    // checks if user specified column names exist in the DataFrame schema
    userSchema.fieldNames.foreach { col =>
      schema.find(f => nameEquality(f.name, col)).getOrElse {
        throw new AnalysisException(
          s"createTableColumnTypes option column $col not found in schema " +
            schema.catalogString)
      }
    }

    val userSchemaMap = userSchema.fields.map(f => f.name -> typeName(f)).toMap
    val isCaseSensitive = sparkSession.sessionState.conf.caseSensitiveAnalysis
    if (isCaseSensitive) userSchemaMap else CaseInsensitiveMap(userSchemaMap)
  }
}
