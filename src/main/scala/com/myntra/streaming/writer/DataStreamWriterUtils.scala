package com.myntra.streaming.writer

import java.util.concurrent.TimeUnit

import com.memsql.spark.connector.MemSQLConnectionPool.DEFAULT_JDBC_LOGIN_TIMEOUT
import com.myntra.streaming.config._
import com.myntra.utils.Constants
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.streaming.{OutputMode, Trigger}

import scala.concurrent.duration._

/*
Created By samathashetty on 20/12/18
*/
object DataStreamWriterUtils {


  def writeJdbc(df: DataFrame, checkPointLocation: String, credential: Credential, outputMode: String, interval: Int, timeUnit: TimeUnit, revenueCalc: Boolean) = {

    df.writeStream.
      format(Constants.STREAM_JDBC).
      option("checkpointLocation", checkPointLocation).
      option("url", credential.url + ":" + credential.port + "/" + credential.dbName).
      option("dbtable", "nrt_admetrics_test").
      option("user", credential.username).
      option("password", credential.password).
      option("revenueCalc", revenueCalc.toString)
//      .option("loginTimeout", "200")
      .outputMode(outputMode)
      .start

  }


  def writeToRedis(df: DataFrame, credential: Credential, redisKey: String, outputMode: OutputMode, interval: Int, timeUnit: TimeUnit) = {
    val writer = new RedisWriter(credential , redisKey)
    df.writeStream
//      .option("table_name", credential.dbName)
//      .option("key_column", "key")
//      .option("redis_key", redisKey)
//      .option("checkpointLocation", "checkPoint" + redisKey)
//      .outputMode(OutputMode.Append())
//      .format(Constants.STREAM_REDIS)
            .foreach(writer)
      .trigger(Trigger.ProcessingTime(interval, timeUnit)).
      outputMode(outputMode).
      start()

  }

}
