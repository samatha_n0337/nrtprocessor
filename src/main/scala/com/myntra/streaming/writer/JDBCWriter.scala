package com.myntra.streaming.writer

/*
Created By samathashetty on 07/11/18
*/

import java.sql.{Connection, DriverManager, Statement}
import java.text.SimpleDateFormat

import com.myntra.streaming.RedisProp
import org.apache.spark.sql._


class JDBCWriter(url: String, user: String, pwd: String, update: Boolean = false) extends ForeachWriter[Row] {

  val driver = "com.mysql.cj.jdbc.Driver"
  var connection: Connection = _
  var statement: Statement = _

  override def open(partitionId: Long, version: Long): Boolean = {
    Class.forName(driver)

    connection = DriverManager.getConnection(url, user, pwd)
    statement = connection.createStatement
    true

  }

  override def process(record: Row): Unit = {
    val uidx = record.getString(record.fieldIndex("uidx"))
    val styleId = record.getString(record.fieldIndex("styleId"))
    val key = "uidxStyle:" + uidx + "%" + styleId

    var revenueRec = record.getDouble(record.fieldIndex("revenue"))


    if (!update) {
      val refId = record.getString(record.fieldIndex("refId"))
      val clicks = record.getLong(record.fieldIndex("clicks"))
      val impressions = record.getLong(record.fieldIndex("impressions"))
      val os = record.getString(record.fieldIndex("os"))
      val is_logged_in = record.getBoolean(record.fieldIndex("is_logged_in"))
      val bucket = record.getLong(record.fieldIndex("bucket"))
      val date = bucket / 10000
      val year = date /10000
      val hour = (bucket % 10000) / 100
      val min = bucket % 100
      val monDate = new SimpleDateFormat("YYYYMMDD").parse(date.toString)
      val month = new SimpleDateFormat("MMM").format(monDate)
      val updt = null

      val sql = "INSERT into udp_test.nrt_admetrics VALUES( '%s', '%s', '%s', %d, %d, %d,%f, %s, %d, %d, %d, %d, %s )".format(refId, os, is_logged_in, bucket,
        clicks, impressions, revenueRec, date, year, hour, min,month, null)
      println(sql)
      statement.executeUpdate(sql)

    }


  }

  override def close(errorOrNull: Throwable): Unit = {
    if (errorOrNull != null)
      println(errorOrNull.getStackTrace)
    connection.close
  }
}

