package com.myntra.streaming.writer

import java.util.Properties
import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

import com.myntra.streaming.config.Config
import com.myntra.streaming.reader.InputReaderFactory
import com.myntra.utils.{DBUtils, JsonParser}
import org.apache.spark.sql.{Dataset, Row, SaveMode}

/*
Created By samathashetty on 22/11/18
*/
class MemSqlBatchWriter {

  def initConnection() = {

    //create properties object
    Class.forName("com.amazon.redshift.jdbc42.Driver")

    val prop = new java.util.Properties
    prop.setProperty("driver", "com.amazon.redshift.jdbc42.Driver")
    prop.setProperty("user", "x")
    prop.setProperty("password", "x")

    //jdbc mysql url - destination database is named "data"
    val url = "jdbc:mysql://52.220.65.55:3306/udp_test"

    //destination database table
    val table = "nrt_admetrics"

  }


  def writeToTable() = {

    val ex = new ScheduledThreadPoolExecutor(1)
    val task = new Runnable {

      def run() = exportToMemSql()

    }
    val f = ex.scheduleAtFixedRate(task, 1, 15, TimeUnit.MINUTES)
    f.cancel(false)

  }

  def exportToMemSql(): Unit = {



    val redisReader = InputReaderFactory.apply("redis")
    val df = redisReader.readFiles("10.0.0.115", "")
    import com.memsql.spark.connector._

    DBUtils.writeToMemSql(df, SaveMode.Append, "nrt_admetrics")


  }



}
