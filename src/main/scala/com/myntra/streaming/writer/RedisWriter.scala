package com.myntra.streaming.writer

import com.myntra.streaming.RedisProp
import com.myntra.streaming.config.Credential
import com.redislabs.provider.redis._
import org.apache.spark.sql.{ForeachWriter, Row}
import redis.clients.jedis.{Connection, Jedis}

/*
Created By samathashetty on 07/11/18
*/
class RedisWriter(credential: Credential, writeType: String) extends ForeachWriter[Row] {

  var conn: Jedis = null
  def open(partitionId: Long, version: Long): Boolean = {
    println("REDIS connection open")

    true
  }


  def process(record: Row) = {
    println("in Process key: " + writeType)
    println(record.schema)

    conn = ConnectionPool.connect(new RedisEndpoint("10.0.6.178", 6379, null , timeout = 2000))
    val keyVal = conn.hgetAll(writeType)

    val key = record.getString(record.fieldIndex("key"))
    val value = record.getString(record.fieldIndex("value"))

    if(key != null && value != null)
      keyVal.put(key, value)
    if(keyVal != null && keyVal.size() >0)
      conn.hmset(writeType, keyVal)

    conn.close()


  }


  def close(errorOrNull: Throwable): Unit = {
    if (errorOrNull != null)
      println("error close:::" + errorOrNull.getMessage)

    println("REDIS connection close")


  }
}
