package com.myntra.utils

/*
Created By samathashetty on 20/12/18
*/
object Constants {
  val STREAM_JDBC = "streamjdbc"
  val STREAM_REDIS = "streamredis"

  val REF_ID_REGEX ="^[0-9]*:[0-9|a-z|A-Z|-]*$"
  val STYLE_ID_REGEX = "^[0-9]*$"

}
