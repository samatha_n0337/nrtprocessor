package com.myntra.utils

import java.util.Properties

import org.apache.spark.sql.{Dataset, Row, SaveMode}

/*
Created By samathashetty on 27/11/18
*/
object DBUtils {

  def writeToMemSql(df: Dataset[Row], mode: SaveMode, tableName: String): Unit =
  {
    val connProp = new Properties()
    connProp.put("user", "root")
    connProp.put("password", "Admin@123!")
    connProp.put("zeroDateTimeBehavior", "CONVERT_TO_NULL")
    connProp.put("tinyInt1isBit", "false")

    df.write.mode(mode).jdbc("jdbc:mysql://10.0.0.165:3306/nrt_test", tableName, connProp)


  }
}
