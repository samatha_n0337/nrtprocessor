package com.myntra.utils

/*
Created By samathashetty on 01/11/18
*/
object EventConstants {
  val STATE_EVENTS = Array("streamCardChildClick","feedCardClick","streamCardClick","feedCardChildClick","Level1LeftNavClick","Level2LeftNavClick","Level3LeftNavClick","RightNavChildClick","searchFired","SearchFired")
  val SOURCE_EVENTS = Array("App Launch","appLaunch","deep - link","DeepLink","push-notification")
  val BANNER_EVENTS = Array("banner-child-click","banner-click","navi-child-click","navi-click")
  val COLUMNS = Array("session_id", "uidx","client_ts","event_type","refId", "styleId", "is_logged_in", "os")
  val SIMILAR_PRODUCTS_EVENTS = Array("Similar products button clicked", "Similar product clicked", "similarProductsClick")
  val LOOK_EVENTS = Array("LookWidgetLoad", "LookBookViewed")
  val LIST_PAGE_EVENT = "product_click_list_page"
  val TOP_NAV_NAMES = Array("Cart","NotificationCentre")
  val CARD_EVENTS = Array("streamCardChildClick","feedCardClick","streamCardClick","feedCardChildClick")
  val MENU_EVENTS = Array("Level1LeftNavClick", "Level2LeftNavClick", "Level3LeftNavClick", "RightNavChildClick")
  val SEARCH_EVENTS = Array("searchFired","SearchFired")

}
