package com.myntra.utils

/*
Created By samathashetty on 20/12/18
*/


import net.liftweb.json.JsonParser.ParseException
import net.liftweb.json._

import scala.io.Source
class EmptyClass

/**
  * Object that parses a JSON into the schema defined by class T.
  * @throws ParseException Can throw exception if JSON schema is not valid.
  */
object JsonParser {
  def parseJson[T <: EmptyClass: Manifest](filePath: String): T = {
    val source = Source.fromFile(filePath)
    val lines = try source.getLines.mkString("\n") finally source.close()
    implicit val formats = net.liftweb.json.DefaultFormats

    val json = parse(lines)
    json.extract[T]
  }
}

