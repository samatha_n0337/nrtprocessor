package com.myntra.utils

/*
Created By samathashetty on 19/12/18
*/
object TimeConstants {

  val YYYY_MM_DD = "yyyyMMdd"
  val YYYY = "YYYY"
  val MMMM = "MMMM"
  val DD = "dd"
  val HOUR_24 = "HH"
  val MM = "mm"


}
