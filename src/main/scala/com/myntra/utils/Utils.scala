package com.myntra.utils

import java.util

import com.redislabs.provider.redis.{ConnectionPool, RedisEndpoint}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp
import org.apache.spark.sql.functions.udf

/*
Created By samathashetty on 18/11/18
*/
class Utils private(var eventsMap: util.Map[String, String], var refIdMap: util.Map[String, String], var revenueMap: util.Map[String, String]) {

  def populateRedisMap() = {

    val jc = ConnectionPool.connect(new RedisEndpoint("10.0.6.178", 6379, null , timeout = 8000))

    eventsMap = jc.hgetAll("eventsEarly")
    refIdMap = jc.hgetAll("sessionRefId")
    revenueMap = jc.hgetAll("revenueEarly")

    jc.close()
  }
}

object Utils {
  private var _instance: Utils = null

  def instance() = {
    if (_instance == null)
      _instance = new Utils(new util.HashMap[String, String], new util.HashMap[String, String], new util.HashMap[String, String])
    _instance

  }


  val indexFromConcatField = udf((concatField: String, separator: String, index: Int) => {
    if (concatField == null)
      null
    else
      concatField.split(separator)(index)
  })




}
